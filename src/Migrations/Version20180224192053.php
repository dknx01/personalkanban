<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180224192053 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task ADD `column` ENUM(\'0\', \'1\', \'2\', \'3\', \'4\') DEFAULT \'4\' NOT NULL COMMENT \'(DC2Type:TaskColumnType)\', CHANGE status status ENUM(\'1\', \'0\', \'2\', \'3\') DEFAULT \'1\' NOT NULL COMMENT \'(DC2Type:TaskStatusType)\'');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task DROP `column`, CHANGE status status ENUM(\'1\', \'0\', \'2\', \'3\') NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:TaskStatusType)\'');
    }
}
