<?php
/**
 * personalkanban
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 25.02.18
 */

namespace App\DTO;

use App\Entity\Task;

class CardAction
{
    public const TYPE_SIMPLE = 0;
    public const TYPE_DYNAMIC_TASK_PARAMETERS = 1;

    /** @var string */
    private $name;
    /** @var array */
    private $classes = [];
    /** @var string */
    private $icon;
    /** @var string */
    private $target;
    /** @var int */
    private $type;
    /** @var array */
    private $parameters;

    /**
     * CardAction constructor.
     * @param string $name
     * @param string $target
     * @param int $type
     * @param array $classes
     * @param array $parameters
     * @param string $icon
     */
    public function __construct(
        string $name,
        string $target,
        $type = self::TYPE_SIMPLE,
        array $classes = [],
        array $parameters = [],
        string $icon = ''
    ) {
        $this->name = $name;
        $this->classes = $classes;
        $this->icon = $icon;
        $this->target = $target;
        $this->type = $type;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getClasses(): array
    {
        return $this->classes;
    }

    /**
     * @return string
     */
    public function getClassesAsString():string
    {
        return implode(' ', $this->getClasses());
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param Task $task
     * @return string
     */
    public function getTarget(Task $task): string
    {
        return $this->target;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}