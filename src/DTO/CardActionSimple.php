<?php
/**
 * personalkanban
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 25.02.18
 */

namespace App\DTO;

class CardActionSimple extends CardAction
{
    /**
     * CardAction constructor.
     * @param string $name
     * @param string $target
     * @param array $classes
     * @param string $icon
     */
    public function __construct(string $name, string $target, array $classes = [], string $icon = '')
    {
        parent::__construct($name, $target, self::TYPE_SIMPLE, $classes, [], $icon);
    }
}