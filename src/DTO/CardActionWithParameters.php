<?php
/**
 * personalkanban
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 25.02.18
 */

namespace App\DTO;

use App\Entity\Task;
use Symfony\Component\Routing\Router;

class CardActionWithParameters extends CardAction
{
    /** @var Router */
    private $router;
    /** @var string[] */
    private static $methodPrefixes = ['get', 'is', 'has'];

    /**
     * CardAction constructor.
     * @param string $name
     * @param string $target
     * @param array $parameters
     * @param array $classes
     * @param string $icon
     */
    public function __construct(string $name, string $target, array $parameters, array $classes = [], string $icon = '')
    {
        parent::__construct($name, $target, self::TYPE_DYNAMIC_TASK_PARAMETERS, $classes, $parameters, $icon);
    }

    /**
     * @param Router $router
     * @return CardActionWithParameters
     */
    public function setRouter(Router $router): CardActionWithParameters
    {
        $this->router = $router;
        return $this;
    }

    /**
     * @inheritdoc
     * @throws \RuntimeException
     */
    public function getTarget(Task $task): string
    {
        $parameters = [];
        foreach ($this->getParameters() as $parameter) {
            foreach (self::$methodPrefixes as $prefix) {
                $method = $prefix . ucfirst($parameter);
                if (method_exists($task, $method)) {
                    $parameters[$parameter] = $task->$method();
                    break;
                }
            }
        }
        return $this->router->generate(parent::getTarget($task), $parameters);
    }
}