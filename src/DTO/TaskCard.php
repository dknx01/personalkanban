<?php
/**
 * personalkanban
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.02.18
 */

namespace App\DTO;

use App\Entity\Task;

class TaskCard
{
    /** @var Task */
    private $task;
    /** @var CardAction[] */
    private $actions = [];

    /**
     * TaskCard constructor.
     * @param Task $task
     * @param CardAction[] $actions
     */
    public function __construct(Task $task, array $actions)
    {
        $this->task = $task;
        $this->actions = $actions;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }

    /**
     * @return CardAction[]
     */
    public function getActions(): array
    {
        return $this->actions;
    }
}