<?php
/**
 * personalkanban
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.02.18
 */

namespace App\DTO;

use App\DBAL\Types\TaskColumnType;
use App\Entity\Task;
use Doctrine\Common\Collections\ArrayCollection;

class TaskCollection
{
    /** @var TaskCard[] */
    private $readyCollection = [];
    /** @var TaskCard[] */
    private $wipCollection = [];
    /** @var TaskCard[] */
    private $onHoldCollection = [];
    /** @var TaskCard[] */
    private $doneCollection = [];
    /** @var TaskCard[] */
    private $newCollection = [];
    /** @var TaskCard[] */
    private $draftCollection = [];

    /**
     * TaskCollection constructor.
     * @param iterable $tasks
     * @param CardAction[] $cardActions
     */
    public function __construct(iterable $tasks, array $cardActions)
    {
        foreach ($tasks as $task) {
            $this->add($task, $cardActions);
        }
    }

    /**
     * @param Task $task
     * @param CardAction[] $cardActions
     */
    public function add(Task $task, array $cardActions):void
    {
        switch ($task->getColumn()) {
            case TaskColumnType::READY:
                $this->readyCollection[] = new TaskCard($task, $cardActions);
                break;
            case TaskColumnType::WIP:
                $this->wipCollection[] = new TaskCard($task, $cardActions);
                break;
            case TaskColumnType::ON_HOLD:
                $this->onHoldCollection[] = new TaskCard($task, $cardActions);
                break;
            case TaskColumnType::DONE:
                $this->doneCollection[] = new TaskCard($task, $cardActions);
                break;
            case TaskColumnType::NEW:
                $this->newCollection[] = new TaskCard($task, $cardActions);
                break;
            case TaskColumnType::DRAFT:
                $this->draftCollection[] = new TaskCard($task, $cardActions);
                break;
        }
    }

    /**
     * @return TaskCard[]
     */
    public function getReadyCollection(): array
    {
        return $this->readyCollection;
    }

    /**
     * @return TaskCard[]
     */
    public function getWipCollection(): array
    {
        return $this->wipCollection;
    }

    /**
     * @return TaskCard[]
     */
    public function getOnHoldCollection(): array
    {
        return $this->onHoldCollection;
    }

    /**
     * @return TaskCard[]
     */
    public function getDoneCollection(): array
    {
        return $this->doneCollection;
    }

    /**
     * @return TaskCard[]
     */
    public function getNewCollection(): array
    {
        return $this->newCollection;
    }

    /**
     * @return TaskCard[]
     */
    public function getDraftCollection(): array
    {
        return $this->draftCollection;
    }
}