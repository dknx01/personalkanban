<?php
/**
 * Created by PhpStorm.
 * User: erik
 * Date: 17.02.18
 * Time: 17:55
 */

namespace App\Controller;

use App\DTO\CardAction;
use App\DTO\CardActionWithParameters;
use App\DTO\TaskCollection;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ContainerRepositoryFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @var TaskRepository
     */
    private $taskRepo;

    /**
     * DefaultController constructor.
     * @param TaskRepository $taskRepo
     */
    public function __construct(TaskRepository $taskRepo)
    {
        $this->taskRepo = $taskRepo;
    }

    /**
     * @Route("/", name="daschboard")
     * @return Response
     */
    public function default()
    {
        /** @var Task[] */
        $tasks = $this->taskRepo->findAll();
        $cardActions = [
            (new CardActionWithParameters(
                'Edit',
                'edit',
                [
                    'id'
                ],
                [
                    'btn-floating',
                    'btn-sm',
                    'waves-effect',
                    'waves-light'
                ],
                'edit'
            ))->setRouter($this->get('router'))
        ];
        return $this->render('Default/default.html.twig', ['cards' => new TaskCollection($tasks, $cardActions)]);
    }
}