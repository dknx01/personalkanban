<?php
/**
 * Created by PhpStorm.
 * User: erik
 * Date: 16.01.18
 * Time: 18:48
 */

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    /**
     * @Route("/task/new", name="new")
     * @return Response
     */
    public function new():Response
    {
        $task = new Task();
        $task->setTitle('Test465');
        $task->setDescription('Test description');
        $task->setStatus(2);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($task);
        $entityManager->flush();

        dump($task);
        return new Response('test dings');
    }

    /**
     * @Route("/task/new/form", name="new_form")
     * @param Request $request
     * @return Response
     */
    public function newForm(Request $request):Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
        }
        return $this->render('Task/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/task/update", name="update")
     * @return Response
     */
    public function update():Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $task = $entityManager->getRepository(Task::class)->find('37a5a5ea-e452-4974-8130-2e681a9836ff');
        $task->setDescription($task->getDescription() . ' bvi rogeorgoehgore oghrog');
        $entityManager->persist($task);
        $entityManager->flush();

        dump($task);
        return new Response('test dings');
    }

    /**
     * @Route("/task/edit/{id}", name="edit")
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function edit($id, Request $request):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $task = $entityManager->getRepository(Task::class)->find($id);

        dump($task);
        $form  = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);


        return $this->render('Task/edit.html.twig', ['form' => $form->createView()]);
    }
}