<?php
/**
 * Created by PhpStorm.
 * User: erik
 * Date: 16.01.18
 * Time: 19:53
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class TaskColumnType extends AbstractEnumType
{
    public const DRAFT = 0;
    public const NEW = 1;
    public const READY = 2;
    public const WIP = 3;
    public const ON_HOLD = 4;
    public const DONE = 5;

    /**
     * @var array
     */
    protected static $choices = [
        self::READY => 'Ready',
        self::WIP => 'WiP',
        self::ON_HOLD => 'On Hold',
        self::DONE => 'Done',
        self::NEW => 'New',
        self::DRAFT => 'Draft'
    ];
}