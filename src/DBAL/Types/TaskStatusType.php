<?php
/**
 * Created by PhpStorm.
 * User: erik
 * Date: 16.01.18
 * Time: 19:53
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class TaskStatusType extends AbstractEnumType
{
    public const DISABLED = 0;
    public const ACTIVE = 1;
    public const RUNNING = 2;
    public const CLOSED = 3;

    /**
     * @var array
     */
    protected static $choices = [
        self::ACTIVE => 'Active',
        self::DISABLED => 'Disabled',
        self::RUNNING => 'Running',
        self::CLOSED => 'Closed'
    ];
}