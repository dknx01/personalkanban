<?php
/**
 * Created by PhpStorm.
 * User: erik
 * Date: 18.01.18
 * Time: 19:54
 */

namespace App\Form;

use App\DBAL\Types\TaskStatusType;
use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Task::class,
                'translation_domain' => false,
                'attr' => [
                    'class' => 'col s12'
                ]
            ]
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $builder->add(
            'title',
            TextType::class
        )
            ->add(
                'description',
                TextareaType::class
            )
            ->add(
                'dueTo',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => [
                        'class' => 'browser-default datepicker'
                    ]
                ]
            )
            ->add(
                'status',
                ChoiceType::class,
                [
                    'choices' => TaskStatusType::getChoices(),
                    'attr' => [
                        'class' => 'browser-default'
                    ],
                ]
            )
            ->add(
                'cancel',
                ButtonType::class
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Create Task'
                ]
            )
        ;
    }
}