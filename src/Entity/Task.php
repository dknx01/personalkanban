<?php
/**
 * Created by PhpStorm.
 * User: erik
 * Date: 14.01.18
 * Time: 20:58
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use App\DBAL\Types\TaskStatusType;

/**
 * Class Tasks
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @ORM\Table()
 */
class Task
{
    /**
     * @ORM\Column(type="guid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $title = '';

    /**
     * @ORM\Column(type="text", nullable=false)
     *
     * @var string
     */
    private $description = '';

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false, name="createdAt")
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, name="completedAt")
     * @var \DateTimeImmutable|null
     */
    private $completedAt;

    /**
     * @ORM\Column(name="status", type="TaskStatusType", nullable=false, options={"default": 1})
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\TaskStatusType")
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="column", type="TaskColumnType", nullable=false, options={"default": 4})
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\TaskColumnType")
     * @var int
     */
    private $column;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, name="dueTo")
     * @var \DateTimeImmutable|null
     */
    private $dueTo;

    /**
     * @ORM\Column(type="datetime", nullable=false, name="updatedAt")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, name="startedAt")
     * @var \DateTimeImmutable|null
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, name="finishedAt")
     * @var \DateTimeImmutable|null
     */
    private $finishedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @var Collection|ArrayCollection
     */
    private $tags;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->status = TaskStatusType::ACTIVE;
        $this->tags = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeImmutable $createdAt
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getCompletedAt():?\DateTimeImmutable
    {
        return $this->completedAt;
    }

    /**
     * @param \DateTimeInterface $completedAt
     */
    public function setCompletedAt(\DateTimeInterface $completedAt): void
    {
        $this->completedAt = $completedAt instanceof \DateTimeImmutable ?:
            \DateTimeImmutable::createFromMutable($completedAt);
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDueTo():?\DateTimeImmutable
    {
        return $this->dueTo;
    }

    /**
     * @param \DateTimeInterface $dueTo
     */
    public function setDueTo(\DateTimeInterface $dueTo): void
    {
        $this->dueTo = $dueTo instanceof \DateTimeImmutable ?: \DateTimeImmutable::createFromMutable($dueTo);
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getStartedAt():?\DateTimeImmutable
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTimeImmutable $startedAt
     */
    public function setStartedAt(\DateTimeImmutable $startedAt): void
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getFinishedAt():?\DateTimeImmutable
    {
        return $this->finishedAt;
    }

    /**
     * @param \DateTimeImmutable $finishedAt
     */
    public function setFinishedAt(\DateTimeImmutable $finishedAt): void
    {
        $this->finishedAt = $finishedAt;
    }

    /**
     * @return int
     */
    public function getColumn(): int
    {
        return $this->column;
    }

    /**
     * @param int $column
     */
    public function setColumn(int $column): void
    {
        $this->column = $column;
    }

    /**
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param Collection $tags
     */
    public function setTags(Collection $tags): void
    {
        $this->tags = $tags;
    }
}