<?php
/**
 * personalkanban
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 26.02.18
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @ORM\Table()
 * @ORM\Entity()
 */
class Tag
{
    /**
     * @ORM\Column(type="guid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @var string
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $color;

    /**
     * Tag constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color = 'grey')
    {
        $this->name = $name;
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }
}