var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');

Encore.addPlugin(
    new CopyWebpackPlugin(
        [
            { from: './assets/MDB Free/css/bootstrap.min.css', to: '../css/bootstrap.min.css' },
            { from: './assets/MDB Free/css/mdb.min.css', to: '../css/mdb.min.css' },
            { from: './assets/MDB Free/js/jquery-3.2.1.min.js', to: '../js/jquery-3.2.1.min.js' },
            { from: './assets/MDB Free/js/bootstrap.min.js', to: '../js/bootstrap.min.js' },
            { from: './assets/MDB Free/js/mdb.min.js', to: '../js/bootstrap.min.js' },
            { from: './assets/MDB Free/js/popper.min.js', to: '../js/popper.min.js' },
            { from: './assets/MDB Free/font/roboto/*', to: '../font/roboto/', flatten: true },
            { from: './assets/MDB Free/img/lightbox/*', to: '../img/lightbox/', flatten: true},
            { from: './assets/MDB Free/img/overlays/*', to: '../img/overlays/', flatten: true},
            { from: './assets/MDB Free/img/svg/*', to: '../img/svg/', flatten: true}
        ]
    )
);

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()
    // show OS notifications when builds finish/fail
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    // .addEntry('materialize', './assets/sass/materialize.scss')

    // uncomment to define the assets of the project
    .createSharedEntry('js/common', ['jquery'])

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
